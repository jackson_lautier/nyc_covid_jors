This public repository contains data for the purposes of replication and supporting future research in relation to the publication:

Cohen, J. P., Friedt, F. L., & Lautier, J. P. (2022). The impact of the Coronavirus pandemic on New York City real estate: First evidence. Journal of Regional Science, 62, 858–888. https://doi.org/10.1111/jors.12591.

**License**

This data is protected by a Creative Commons Attribution (CC BY) license. This license allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, so long as attribution is given to the creator. This license allows for commercial use.  For more details, see https://creativecommons.org/licenses/by/4.0/.
  

**Data Notes**

All data notes relate to the published version: https://doi.org/10.1111/jors.12591.

The files "Table3_all_sales.dta" and "Table3_repeat_sales.dta" were used to generate the results for Table 3.

The file "Table4_repeat_sales.dta" was used to generate the results for Table 4.

The file "Table5_repeat_sales.dta" was used to generate the results for Table 5.

**Disclaimer**

The authors have made a good faith attempt to verify these data files are correct and the published results are replicable.  Nonetheless, it is the responsibility of reusers to ensure their accuracy in its future use.



